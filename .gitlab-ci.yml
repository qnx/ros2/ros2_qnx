stages:
  - build

.build:
  stage: build
  variables:
    LANG: "en_US.UTF-8"
    LANGUAGE: "en_US.UTF-8"
    LC_ALL: "en_US.UTF-8"
  before_script:
    - |
      echo "Installing cmake3.18"
      apt update && apt install -y wget
      wget https://cmake.org/files/v3.18/cmake-3.18.0-Linux-x86_64.sh
      sh cmake-3.18.0-Linux-x86_64.sh --prefix=/usr/local --skip-license
      cmake --version
    - |
      echo 'Etc/UTC' > /etc/timezone
      ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime
      apt-get update
      apt-get install -q -y --no-install-recommends tzdata locales curl gnupg2
    - |
      locale-gen ${LANG}; dpkg-reconfigure -f noninteractive locales
      apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
      echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu focal main" > /etc/apt/sources.list.d/ros2-latest.list
    - |
      echo "Installing Build tools"
      apt update && apt install -y \
        build-essential \
        git \
        python3-colcon-common-extensions \
        python3-flake8 \
        python3-pip \
        python3-pytest-cov \
        python3-rosdep \
        python3-setuptools \
        python3-vcstool \
        bc \
        subversion \
        autoconf \
        libtool-bin \
        libssl-dev \
        zlib1g-dev \
        rsync \
        rename
    - |
      python3 -m pip install -U \
        argcomplete \
        flake8-blind-except \
        flake8-builtins \
        flake8-class-newline \
        flake8-comprehensions \
        flake8-deprecated \
        flake8-docstrings \
        flake8-import-order \
        flake8-quotes \
        pytest-repeat \
        pytest-rerunfailures \
        pytest \
        Cython \
        numpy \
        lark-parser
    - |
      echo "Installing CycloneDDS tools"
      apt install -y bison;
      cd ~/
      git clone https://github.com/eclipse-cyclonedds/cyclonedds.git
      cd cyclonedds
      git checkout 6e16753f971049061a27fd70c70e2d780ff321ef
      mkdir build
      cd build
      cmake ..
      cmake --build . --target ddsconf idlc
      export DDSCONF_EXE=$(find ~/cyclonedds -type f -name ddsconf)
      export IDLC_EXE=$(find ~/cyclonedds -type f -name idlc)
    - |
      echo "Downloading QNX Software Center ..."
      cd ~/
      mkdir ~/.qnx
      curl -v --cookie-jar ~/.qnx/myqnxcookies.auth --form "userlogin=$MYQNX_USER" --form "password=$MYQNX_PASSWORD" -X POST https://www.qnx.com/account/login.html > login_response.html
      curl -v -L --cookie ~/.qnx/myqnxcookies.auth https://www.qnx.com/download/download/58067/qnx-setup-1.7-202103252036-linux.run > qnx-setup-lin.run
      chmod a+x qnx-setup-lin.run
      ./qnx-setup-lin.run force-override disable-auto-start agree-to-license-terms ~/qnxinstall
    - |
      echo "Installing License ..." 
      ~/qnxinstall/qnxsoftwarecenter/qnxsoftwarecenter_clt -syncLicenseKeys -myqnx.user="$MYQNX_USER" -myqnx.password="$MYQNX_PASSWORD" -addLicenseKey $LICENSE_KEY
      echo "Downloading QNX SDP ..."  
      ~/qnxinstall/qnxsoftwarecenter/qnxsoftwarecenter_clt -mirror -cleanInstall -destination ~/qnx710 -installBaseline com.qnx.qnx710 -myqnx.user="$MYQNX_USER" -myqnx.password="$MYQNX_PASSWORD"      
    - |
      mkdir ~/ros2_rolling
      echo "Copying QNX Dependency Build Files ..."
      cd ~/ros2_rolling
      rsync -haz "$CI_PROJECT_DIR/" .
      mkdir -p ~/ros2_rolling/src
      vcs import src < ros2.repos
      ./patch.sh
      mkdir -p src/qnx_deps
      vcs import src/qnx_deps < qnx_deps.repos
  script:
    - |
      echo "Building ROS2 Source ..."
      ./patch-pkgxml.py --path=src
      ./colcon-ignore.sh
      . ~/qnx710/qnxsdp-env.sh
      ./build-ros2.sh

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

Build x86_64 QNX 7.1:
  image: ubuntu:focal
  extends:
    - .build
  variables:
    CPU: "x86_64"
    
Build aarch64 QNX 7.1:
  image: ubuntu:focal
  extends:
    - .build
  variables:
    CPU: "aarch64"
